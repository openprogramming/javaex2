package pl.op.walker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public class DuplicateBuilder {
	
	HashMap<String, ArrayList<Duplicate>> m_Duplicates = new HashMap<String, ArrayList<Duplicate>>();
	
	public class Duplicate
    {
		public Duplicate(){}
        String name;
        File file;
    }

	public void check(String file, String absolutePath)
    {
        if(file.length() > 0)
        {
            Duplicate d = new Duplicate();
            d.name = file;
            d.file = new File(absolutePath);

            if(!m_Duplicates.containsKey(file))
                m_Duplicates.put(file, new ArrayList<Duplicate>());

            ArrayList<Duplicate> list = m_Duplicates.get(file);
            list.add(d);
        }
    }
	
	public void all() throws FileNotFoundException 
	{
        StringBuilder sb = new StringBuilder();
        
        for(Entry<String, ArrayList<Duplicate>> entry : m_Duplicates.entrySet())
        {
            if(entry.getValue().size() == 1)
                continue;

            sb.append("Duplicate entry: ").append(entry.getKey()).append(" [x")
            .append(entry.getValue().size()).append("]\n");
            for(int i = 0; i < entry.getValue().size(); ++i)
            {
                sb.append("\nPath: " + entry.getValue().get(i).file.getAbsolutePath() + "\n");
            }

            sb.append("\n\n");
        }

        System.out.println(sb.toString());
		PrintWriter  fs = new PrintWriter("./log.txt");
		fs.print(sb.toString());
		fs.close();
	}

}
